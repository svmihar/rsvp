import pandas as pd
from generator.id import generate_id

contoh_nama = [
    "Aaron",
    "Ab",
    "Abba",
    "Abbe",
    "Abbey",
    "Abbie",
    "Abbot",
    "Abbott",
    "Abby",
    "Abdel",
    "Abdul",
    "Abe",
    "Abel",
    "Abelard",
    "Abeu",
    "Abey",
    "Abie",
    "Abner",
    "Abraham",
]

contoh_id = (generate_id() for _ in range(len(contoh_nama)))

df = pd.DataFrame(list(zip(contoh_nama, contoh_id)), columns=["nama", "id"])
df.to_csv("contoh_data.csv", index=False)
