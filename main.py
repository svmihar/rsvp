from generator import qr, id
import pandas as pd
from tqdm import tqdm
import argparse
import pdb


QR_FOLDER = "qr_images"


df = pd.read_csv("contoh_data.csv") # quasi database, TODO: migrate to a proper db



def tambah_tamu_baru_single(nama, df):
    tamu = {"nama": nama, "id": id.generate_id()}
    df = df.append(tamu, ignore_index=True)
    print(nama, "ditambahkan")
    df.to_csv("contoh_data.csv", index=False)


def tamu_baru_csv(csv_path):
    df = pd.read_csv(csv_path)
    assert "id" in df.columns
    print(f"generating {len(df)} qr code tamu")
    for id_ in tqdm(df["id"].values):
        qr.generate(id_, f"{QR_FOLDER}/{id_}.png")


def get_detail(id_, df):
    tamu = df["id"].values == id_
    hasil = df[tamu].to_dict("records")
    if hasil: 
        return hasil

    return {'message': 'tamu not found'}


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--tambah_tamu", type=str)  # single tamu
    parser.add_argument("--generate_qr", type=str)  # csv
    parser.add_argument("--id", type=str)  # ambil tamu by id
    args = parser.parse_args()
    if args.tambah_tamu:
        tambah_tamu_baru_single(args.tambah_tamu, df)
    if args.generate_qr:
        tamu_baru_csv(args.generate_qr)
    if args.id:
        print(get_detail(args.id, df))
