from qrcode import make
import cv2


def generate(data, img_name):
    img = make(data)
    img.save(img_name)
    return img
