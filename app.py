from flask import Flask, jsonify
from flask_cors import CORS
from main import get_detail, df

app = Flask(__name__)
CORS(app)



@app.route('/')
def index():
    return 'welcome'

@app.route("/tamu/<id>", methods=['GET'])
def get_user_by_id(id): 
    tamu = get_detail(id, df)
    return jsonify(tamu)
@app.route("/alltamu", methods=['GET', 'OPTIONS'])
def get_all_tamu(df=df): 
    return jsonify(df.to_dict('records'))

if __name__ == "__main__": 
    app.run(debug=True, host='0.0.0.0')

